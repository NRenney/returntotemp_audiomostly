** Returning to the Fundamentals on Temperament (in digital systems) **

LaTeX source for paper

Conference: Audio Mostly - submitted 31/05/18

Build:
```
pdflatex ReturnToTemp
bibtex ReturnToTemp
pdflatex ReturnToTemp
pdflatex ReturnToTemp
```

TODO:

- [x] CCS codes need to be updated
- [x] Keep the draft version do not distribute?
- [ ] Probably add more references
- [x] Equations dont allign in SigConf version
- [x] check Meta data stuff 
